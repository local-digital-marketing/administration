import './app.scss';

import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { orange500, orange700 } from 'material-ui/styles/colors';
import AppBar from 'material-ui/AppBar'
import DrawerContainer from '../containers/DrawerContainer';

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: orange500,
        primary2Color: orange700,
        pickerHeaderColor: orange500,
    }
});


const AppComponent = ({showDrawer, onExpandNavigationClick}) => (
    <MuiThemeProvider muiTheme={muiTheme}>
        <div>
            <DrawerContainer/>
            <AppBar
                title="LoDiMa"
                onLeftIconButtonTouchTap={onExpandNavigationClick}
            />
        </div>
    </MuiThemeProvider>
);

AppComponent.propTypes = {
    showDrawer: React.PropTypes.bool.isRequired,
    onExpandNavigationClick: React.PropTypes.func.isRequired
};

export default AppComponent;

