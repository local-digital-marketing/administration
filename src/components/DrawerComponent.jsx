import React from 'react';
import Drawer from 'material-ui/Drawer';

const DrawerComponent = ({open, onRequestChange}) => (
    <Drawer
        docked={false}
        width={200}
        open={open}
        onRequestChange={onRequestChange}
    >
    </Drawer>
);

Drawer.propTypes = {
    open: React.PropTypes.bool.isRequired,
    onRequestChange: React.PropTypes.func.isRequired
};

export default DrawerComponent;