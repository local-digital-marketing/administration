import { combineReducers } from 'redux';
import drawer from './drawer';

const app = combineReducers({
    drawer
});

export default app;