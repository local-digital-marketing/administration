export default (state = { open: false }, action) => {
    switch (action.type) {
        case 'TOGGLE_DRAWER':
            console.log(action);
            return {
                open: !state.open
            };
        default:
            return state;
    }
}