import { connect } from 'react-redux';
import DrawerComponent from '../components/DrawerComponent';
import { toggleDrawer } from '../actions';

const mapStateToProps = (state) => {
    return {
        open: state.drawer.open
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onRequestChange: (open) => {
            dispatch(toggleDrawer(open))
        }
    }
};

const DrawerContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(DrawerComponent);

export default DrawerContainer;