import { connect } from 'react-redux';
import AppComponent from '../components/AppComponent';
import { toggleDrawer } from '../actions';

const mapStateToProps = (state) => {
    return {
        showDrawer: state.drawer.open
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onExpandNavigationClick: (open) => {
            dispatch(toggleDrawer(open))
        }
    }
};

const AppContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(AppComponent);

export default AppContainer;