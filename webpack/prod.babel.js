import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import path from 'path';

export default {
    devtool: 'source-map',
    entry: path.join(__dirname, '..', 'src', 'index.jsx'),
    resolve: {
        extensions: ['.js', '.jsx']
    },
    output: {
        filename: 'app.bundle.js',
        path: path.join(__dirname, '..',  'dist')
    },
    module: {
        loaders: [
            {
                test: /\.(js|jsx)$/,
                loader: 'babel-loader',
                exclude: 'node_modules'
            }
        ]
    },
    plugins: [
        //new webpack.optimize.UglifyJsPlugin(),
        new HtmlWebpackPlugin({
            template: path.join(__dirname, '..',  'src', 'index.prod.html'),
            filename: 'index.html'
        })
    ]
}