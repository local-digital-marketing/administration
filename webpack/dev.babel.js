import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import path from 'path';

export default {
    devtool: 'eval',
    entry: path.join(__dirname, '..', 'src', 'index.jsx'),
    resolve: {
        extensions: ['.js', '.jsx']
    },
    output: {
        filename: 'app.bundle.js',
        path: path.join(__dirname, '..',  'dist')
    },
    devServer: {
        historyApiFallback: true,
        host: '0.0.0.0',
        port: 8080,
        stats: {
            colors: true
        }
    },
    module: {
        loaders: [
            {
                test: /\.(js|jsx)$/,
                loader: 'babel-loader',
                exclude: 'node_modules'
            },
            {
                test: /\.scss$/,
                loader: 'style-loader!css-loader!sass-loader'
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(__dirname, '..',  'src', 'index.dev.html'),
            filename: 'index.html'
        })
    ]
}